import sys

def compute_character( iterations, max_iterations ) :
    if iterations > 32:
        return " "
    if iterations > 16:
        return "."
    if iterations > 8:
        return "o"
    if iterations > 4:
        return "O"
    return "X"

pix_real = -40
pix_imag = 19
z_real = 0.0
z_imag = 0.0
w_real = 0.0
w_imag = 0.0
c_real = 0.0
c_imag = 0.0
bailout = False
iteration = 0
max_iterations = 1000000

while pix_imag >= -20:
    c_imag = pix_imag / 20.0
    pix_real = -40
    while pix_real <= 39:
        #bailout = False
        c_real = pix_real / 40.0 -.5
        z_real = 0.0
        z_imag = 0.0
        #sys.stdout.write("X")
        iteration = 0
        while iteration < max_iterations:
            w_real = z_real * z_real - z_imag * z_imag + c_real
            w_imag = 2 * z_real * z_imag + c_imag
            z_real = w_real
            z_imag = w_imag
            if z_real * z_real + z_imag * z_imag >= 4:
                break
            iteration += 1
        sys.stdout.write(compute_character(iteration, max_iterations))
        pix_real += 1
    pix_imag -= 1
    print



